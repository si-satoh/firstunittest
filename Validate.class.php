<?php
// 入力チェック定義クラス
class Validate {
    // 最大値チェック
    public static function chkLength($value, $max) {
        return (strlen($value) <= $max) ? true : false;
    }
    // 最大値チェック
    public static function chkLength2($value, $max) {
        return (mb_strlen($value, 'sjis-win') <= $max) ? true : false;
    }
    // 最小値チェック
    public static function chkMinLength($value, $min) {
        return ((strlen($value) >= $min) || ($value == null)) ? true : false;
    }
    // 最小値チェック
    public static function chkMinLength2($value, $min) {
        return ((mb_strlen($value, 'sjis-win') >= $min) || ($value == null)) ? true : false;
    }

    // 必須チェック
    public static function chkRequire($value) {
		$lbReturn = false;
		if (isset($value)) {
			if (is_array($value)) {
				$lbReturn = true;
			} elseif (strlen(trim($value))!=0) {
				$lbReturn = true;
			}
		}
		return $lbReturn;
    }

    // 電話番号チェック
    public static function chkTelNo($value) {
        return (preg_match("/^[0-9]+-[0-9]+-[0-9]+$/", $value) || $value == null) ? true : false;
    }

    // 郵便番号チェック
    public static function chkPostalCode($value) {
        return (preg_match("/^[0-9]{3}-[0-9]{4}$/", $value) || $value == null) ? true : false;
    }

    // サイズチェック
    public static function chkFixSize($value, $fixsize) {
        return (strlen($value) == $fixsize) ? true : false;
    }

    // 数値チェック
    public static function chkNumeric($value) {
        return (is_numeric($value) || $value == null) ? true : false;
    }

    // 半角英数チェック
    public static function chkEngNum($value) {
        // return (ereg("^[:alnum:]*$",$value) || $value == null) ? true : false;
        return (preg_match("/^[0-9a-zA-Z]+$/", $value) || $value == null) ? true : false;
    }

    // 日付妥当性チェック
    public static function chkDate($value) {
        if (is_numeric(strpos($value, "/"))) {
            $liDate = explode("/", $value);
        }
        if (is_numeric(strpos($value, "-"))) {
            $liDate = explode("-", $value);
        }
        if (! is_numeric($liDate[0]) || ! is_numeric($liDate[1]) || ! is_numeric($liDate[2])) {
            return false;
        }
        return checkdate($liDate[1], $liDate[2], $liDate[0]);
    }
    // 時間妥当性チェック
    public static function chkTime($value) {
        if (is_numeric(strpos($value, ":"))) {
            $liTime = explode(":", $value);
        }
        if (! is_numeric($liTime[0]) || ! is_numeric($liTime[1])) {
            return false;
        }
        if ($liTime[0] < 0 || $liTime[0] > 23) {
            return false;
        }
        if ($liTime[1] < 0 || $liTime[1] > 59) {
            return false;
        }

        return true;
    }

    /**
     * メールアドレスとして文字列チェック
     * ただしRFC準拠ではない
     * @param unknown $value
     * @return boolean
     */
    public static function chkMailAddress($value) {
        return (preg_match("/^[0-9a-zA-Z_\.\-\+\/\?]+@[0-9a-zA-Z_\.\-]+\.[0-9a-zA-Z]+$/", $value) || $value == null) ? true : false;
    }

    // タグチェック
    public static function chkHTMLTag($value) {
        $lbRetuen = false;
        $laTag = array(
            "<",
            ">"
        );
        foreach ($laTag as $data) {
            if (is_numeric(strpos($value, $data))) {
                $lbRetuen = true;
            }
        }
        return $lbRetuen;
    }

    // 絵文字チェック
    public static function chkEmoji($value) {
        mb_substitute_character("long");
        $laStr = mb_convert_encoding($value, "EUC-JP", "SJIS");
        if (strpos($laStr, 'BAD+') === false) {
            $lbRetuen = false;
        } else {
            $lbRetuen = true;
        }
        return $lbRetuen;
    }

    /**
     * ディレクトリとして文字列チェック
     *
     * @param unknown $value
     * @param string $permitUpper : '../'を許容するか
     */
    public static function chkDirectory($value, $permitUpper = false) {

        // 文字のチェック
        $valueCheck = (preg_match("/^[0-9a-zA-Z_\.\-\/]+$/", $value) == 1);
        if (! $permitUpper) {
            // '../'（上階層へ移動）チェック
            $upperCheck = (preg_match("/\.{2,}\//", $value) == 0);
        } else {
            $upperCheck = true;
        }
        return $valueCheck && $upperCheck;
    }

    /**
     * NGワードチェック
     * TODO ページによってNGワードを使い分ける？
     *
     * @param unknown $text
     * @return boolean|unknown
     */
    public static function chkNGWord($text) {
        $NG_WORDS=array('ブルー','ミント','オレンジ','シトラス');

        foreach ($NG_WORDS as $ngWord) {
            if (preg_match("/.*" . $ngWord . ".*/", $text)) {
                return false;
            }
        }
        return true;
    }
}
?>
